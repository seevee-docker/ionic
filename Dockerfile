FROM seevee/cordova:alpine

LABEL authors="Maik Hummel <m@ikhummel.com>, Chris Vincent <seevee@pm.me>"

ENV IONIC_VERSION 4.1.1

RUN apk add --no-cache git bzip2 openssh-client && \
    npm i -g --unsafe-perm ionic@${IONIC_VERSION} && \
    ionic --no-interactive config set -g daemon.updates false && \
    apk del git bzip2 openssh-client
